# Translation of kfind to Norwegian Bokmål
#
# Knut Yrvin <knut.yrvin@gmail.com>, 2002, 2003, 2005.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2002, 2007, 2008, 2009, 2010, 2011.
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2004.
# Axel Bojer <fri_programvare@bojer.no>, 2005, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kfindpart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-30 00:49+0000\n"
"PO-Revision-Date: 2011-07-14 21:34+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kfinddlg.cpp:31
#, kde-format
msgctxt "@title:window"
msgid "Find Files/Folders"
msgstr "Finn filer og mapper"

#: kfinddlg.cpp:48 kfinddlg.cpp:197
#, kde-format
msgctxt "the application is currently idle, there is no active search"
msgid "Idle."
msgstr "Tomgang."

#. i18n as below
#: kfinddlg.cpp:133
#, kde-format
msgid "0 items found"
msgstr ""

#: kfinddlg.cpp:172
#, kde-format
msgid "Searching..."
msgstr "Søker …"

#: kfinddlg.cpp:199
#, kde-format
msgid "Canceled."
msgstr "Avbrutt."

#: kfinddlg.cpp:201 kfinddlg.cpp:204 kfinddlg.cpp:207
#, kde-format
msgid "Error."
msgstr "Feil."

#: kfinddlg.cpp:202
#, kde-format
msgid "Please specify an absolute path in the \"Look in\" box."
msgstr "Vennligst angi en fullstendig sti i feltet «Let i»."

#: kfinddlg.cpp:205
#, kde-format
msgid "Could not find the specified folder."
msgstr "Fant ikke den oppgitte katalogen."

#: kfinddlg.cpp:228 kfinddlg.cpp:252
#, kde-format
msgid "one item found"
msgid_plural "%1 items found"
msgstr[0] ""
msgstr[1] ""

#: kfindtreeview.cpp:49
msgid "Read-write"
msgstr "Lese-skrive"

#: kfindtreeview.cpp:50
msgid "Read-only"
msgstr "Bare lese"

#: kfindtreeview.cpp:51
msgid "Write-only"
msgstr "Bare skrive"

#: kfindtreeview.cpp:52
msgid "Inaccessible"
msgstr "Utilgjengelig"

#: kfindtreeview.cpp:72
#, kde-format
msgctxt "file name column"
msgid "Name"
msgstr "Navn"

#: kfindtreeview.cpp:74
#, kde-format
msgctxt "name of the containing folder"
msgid "In Subfolder"
msgstr "I undermappe"

#: kfindtreeview.cpp:76
#, kde-format
msgctxt "file size column"
msgid "Size"
msgstr "Størrelse"

#: kfindtreeview.cpp:78
#, kde-format
msgctxt "modified date column"
msgid "Modified"
msgstr "Endret"

#: kfindtreeview.cpp:80
#, kde-format
msgctxt "file permissions column"
msgid "Permissions"
msgstr "Rettigheter"

#: kfindtreeview.cpp:82
#, kde-format
msgctxt "first matching line of the query string in this file"
msgid "First Matching Line"
msgstr "Første linje med treff"

#: kfindtreeview.cpp:364
#, kde-format
msgid "&Open containing folder(s)"
msgstr "&Åpne omliggende mappe(r)"

#: kfindtreeview.cpp:368
#, kde-format
msgid "&Delete"
msgstr "&Slett"

#: kfindtreeview.cpp:372
#, kde-format
msgid "&Move to Trash"
msgstr "Flytt til &papirkurven"

#: kfindtreeview.cpp:491
#, kde-format
msgctxt "@title:window"
msgid "Save Results As"
msgstr "Lagre resultater som"

#: kfindtreeview.cpp:516
#, kde-format
msgid "Unable to save results."
msgstr "Klarte ikke å lagre resultatet."

#: kfindtreeview.cpp:534
#, kde-format
msgid "KFind Results File"
msgstr "KFind-resultatfil"

#: kfindtreeview.cpp:549
#, kde-format
msgctxt "%1=filename"
msgid "Results were saved to: %1"
msgstr "Resultater ble lagret i: %1"

#: kfindtreeview.cpp:647 kftabdlg.cpp:396
#, kde-format
msgid "&Properties"
msgstr "&Egenskaper"

#: kftabdlg.cpp:66
#, kde-format
msgctxt "this is the label for the name textfield"
msgid "&Named:"
msgstr "Med &navn:"

#: kftabdlg.cpp:69
#, kde-format
msgid "You can use wildcard matching and \";\" for separating multiple names"
msgstr "Du kan bruke jokertegn, og «;» som skille mellom flere navn"

#: kftabdlg.cpp:75
#, kde-format
msgid "Look &in:"
msgstr "Let &i:"

#: kftabdlg.cpp:78
#, kde-format
msgid "Include &subfolders"
msgstr "Ta med &undermapper"

#: kftabdlg.cpp:79
#, kde-format
msgid "Case s&ensitive search"
msgstr "Skill m&ellom små og store bokstaver"

#: kftabdlg.cpp:80
#, kde-format
msgid "&Browse..."
msgstr "&Bla gjennom …"

#: kftabdlg.cpp:81
#, kde-format
msgid "&Use files index"
msgstr "Br&uk fil-indeks"

#: kftabdlg.cpp:82
#, kde-format
msgid "Show &hidden files"
msgstr "Vis &skjulte filer"

#: kftabdlg.cpp:101
#, kde-format
msgid ""
"<qt>Enter the filename you are looking for. <br />Alternatives may be "
"separated by a semicolon \";\".<br /><br />The filename may contain the "
"following special characters:<ul><li><b>?</b> matches any single character</"
"li><li><b>*</b> matches zero or more of any characters</li><li><b>[...]</b> "
"matches any of the characters between the braces</li></ul><br />Example "
"searches:<ul><li><b>*.kwd;*.txt</b> finds all files ending with .kwd or ."
"txt</li><li><b>go[dt]</b> finds god and got</li><li><b>Hel?o</b> finds all "
"files that start with \"Hel\" and end with \"o\", having one character in "
"between</li><li><b>My Document.kwd</b> finds a file of exactly that name</"
"li></ul></qt>"
msgstr ""
"<qt>Oppgi filnavnet du søker etter. <br />Du kan skille alternativer med "
"semikolon «;».<br /><br /> Filnavnet kan innholde følgende spesialtegn:"
"<ul><li><b>?</b> som treffer ethvert enkelt tegn</li><li><b>*</b> treffer "
"null eller flere av alle typer tegn</li><li><b>[…]</b> treff et av  tegnene "
"i firkantparentesene </li></ul><br />Eksempler på søk:<ul><li><b> *.kwd;*."
"txt</b> finner alle filer som ender med .kwd eller .txt</li><li><b>go[dt]</"
"b> finner god og got</li><li><b>Hel?o</b> finner alle  filer som starter med "
"«Hel» og avsluttes med «o», og har bare et tegn i mellom</li><li><b>My "
"Document.kwd</b> finner en fil med akkurat det navnet</li></ul></qt>"

#: kftabdlg.cpp:122
#, kde-format
msgid ""
"<qt>This lets you use the files' index created by the <i>slocate</i> package "
"to speed-up the search; remember to update the index from time to time "
"(using <i>updatedb</i>).</qt>"
msgstr ""
"<qt>Gir tilgang til fil-liste laget av <i>slocate</i>-pakken som gir økt "
"tempo på filsøk. Ikke glem å oppdatere lista fra tid til annen (bruk "
"<i>updatedb</i>).</qt>"

#: kftabdlg.cpp:166
#, kde-format
msgid "Find all files created or &modified:"
msgstr "Finn alle filer laget eller &endret:"

#: kftabdlg.cpp:168
#, kde-format
msgid "&between"
msgstr "&mellom"

#: kftabdlg.cpp:170
#, kde-format
msgid "and"
msgstr "og"

#: kftabdlg.cpp:192
#, kde-format
msgid "File &size is:"
msgstr "Fil&størrelsen er:"

#: kftabdlg.cpp:205
#, kde-format
msgid "Files owned by &user:"
msgstr "Filer som br&uker eier:"

#: kftabdlg.cpp:210
#, kde-format
msgid "Owned by &group:"
msgstr "Filer som tilhører &gruppe:"

#: kftabdlg.cpp:213
#, kde-format
msgctxt "file size isn't considered in the search"
msgid "(none)"
msgstr "(ingen)"

#: kftabdlg.cpp:214
#, kde-format
msgid "At Least"
msgstr "I det minste"

#: kftabdlg.cpp:215
#, kde-format
msgid "At Most"
msgstr "Ikke mer enn"

#: kftabdlg.cpp:216
#, kde-format
msgid "Equal To"
msgstr "Lik"

#: kftabdlg.cpp:218 kftabdlg.cpp:838
#, kde-format
msgid "Byte"
msgid_plural "Bytes"
msgstr[0] "byte"
msgstr[1] "byte"

#: kftabdlg.cpp:219
#, kde-format
msgid "KiB"
msgstr "KiB"

#: kftabdlg.cpp:220
#, kde-format
msgid "MiB"
msgstr "MiB"

#: kftabdlg.cpp:221
#, kde-format
msgid "GiB"
msgstr "GiB"

#: kftabdlg.cpp:284
#, kde-format
msgctxt "label for the file type combobox"
msgid "File &type:"
msgstr "Fil&type:"

#: kftabdlg.cpp:289
#, kde-format
msgid "C&ontaining text:"
msgstr "Inneh&older tekst:"

#: kftabdlg.cpp:295
#, kde-format
msgid ""
"<qt>If specified, only files that contain this text are found. Note that not "
"all file types from the list above are supported. Please refer to the "
"documentation for a list of supported file types.</qt>"
msgstr ""
"<qt> Om valgt, finner bare filer som inneholder denne teksten. Merk ikke "
"alle filtyper fra lista over er støttet. Se i dokumentasjonen for støttede "
"filtyper.</qt>"

#: kftabdlg.cpp:303
#, kde-format
msgid "Case s&ensitive"
msgstr "Skill m&ellom store og små bokstaver"

#: kftabdlg.cpp:304
#, kde-format
msgid "Include &binary files"
msgstr "Ta med &binærfiler"

#: kftabdlg.cpp:307
#, kde-format
msgid ""
"<qt>This lets you search in any type of file, even those that usually do not "
"contain text (for example program files and images).</qt>"
msgstr ""
"<qt> Gir søk i hvilken som helst fil, selv om filene ikke inneholder tekst "
"(for eksempel et program eller bilder).</qt>"

#: kftabdlg.cpp:314
#, kde-format
msgctxt "as in search for"
msgid "fo&r:"
msgstr "ette&r:"

#: kftabdlg.cpp:316
#, kde-format
msgid "Search &metainfo sections:"
msgstr "Søk i filenes &metadata:"

#: kftabdlg.cpp:320
#, kde-format
msgid "All Files & Folders"
msgstr "Alle filer og mapper"

#: kftabdlg.cpp:321
#, kde-format
msgid "Files"
msgstr "Filer"

#: kftabdlg.cpp:322
#, kde-format
msgid "Folders"
msgstr "Mapper"

#: kftabdlg.cpp:323
#, kde-format
msgid "Symbolic Links"
msgstr "Symbolske lenker"

#: kftabdlg.cpp:324
#, kde-format
msgid "Special Files (Sockets, Device Files, ...)"
msgstr "Spesielle filer (sokler, enhetsfiler, …)"

#: kftabdlg.cpp:325
#, kde-format
msgid "Executable Files"
msgstr "Kjørbare filer"

#: kftabdlg.cpp:326
#, kde-format
msgid "SUID Executable Files"
msgstr "SUID-programfiler"

#: kftabdlg.cpp:327
#, kde-format
msgid "All Images"
msgstr "Alle bilder"

#: kftabdlg.cpp:328
#, kde-format
msgid "All Video"
msgstr "Alle videoer"

#: kftabdlg.cpp:329
#, kde-format
msgid "All Sounds"
msgstr "Alle lyder"

#: kftabdlg.cpp:394
#, kde-format
msgid "Name/&Location"
msgstr "Navn/p&lassering"

#: kftabdlg.cpp:395
#, kde-format
msgctxt "tab name: search by contents"
msgid "C&ontents"
msgstr "Innh&old"

#: kftabdlg.cpp:400
#, kde-format
msgid ""
"<qt>Search within files' specific comments/metainfo<br />These are some "
"examples:<br /><ul><li><b>Audio files (mp3...)</b> Search in id3 tag for a "
"title, an album</li><li><b>Images (png...)</b> Search images with a special "
"resolution, comment...</li></ul></qt>"
msgstr ""
"<qt> Søk inne i filenes spesifikke kommentarer/metadata.<br /> Noen "
"eksempler:<br /> <ul> <li><b>Lydfiler (mp3 …)</b> Søk i id3-tagger etter en "
"tittel, et album</li> <li><b> Bilder (png …)</b> Søk etter bilder med en "
"spesiell kommentar, oppløsning etc</li></ul></qt>"

#: kftabdlg.cpp:408
#, kde-format
msgid ""
"<qt>If specified, search only in this field<br /><ul><li><b>Audio files "
"(mp3...)</b> This can be Title, Album...</li><li><b>Images (png...)</b> "
"Search only in Resolution, Bitdepth...</li></ul></qt>"
msgstr ""
"<qt>Søk bare i dette feltet, hvis oppgitt <br /> <ul> <li><b>Lydfiler "
"(mp3 …)</b> Dette kan være TItel, Album …</li> <li><b>Bilder (png …)</b> Søk "
"bare i Oppløsning, Bitdybde … </li> </ul></qt>"

#: kftabdlg.cpp:549
#, kde-format
msgid "Unable to search within a period which is less than a minute."
msgstr "Kan ikke søke innenfor en periode kortere enn ett minutt."

#: kftabdlg.cpp:560
#, kde-format
msgid "The date is not valid."
msgstr "Datoen er ikke gyldig."

#: kftabdlg.cpp:562
#, kde-format
msgid "Invalid date range."
msgstr "Dato-området er ikke gyldig."

#: kftabdlg.cpp:564
#, kde-format
msgid "Unable to search dates in the future."
msgstr "Kan ikke søke etter framtidige datoer."

#: kftabdlg.cpp:637 kftabdlg.cpp:639
#, kde-format
msgid "Size is too big. Set maximum size value?"
msgstr "For stor … Vil du velge en øvre størrelsesgrense?"

#: kftabdlg.cpp:637 kftabdlg.cpp:639
#, kde-format
msgid "Error"
msgstr "Feil"

#: kftabdlg.cpp:637 kftabdlg.cpp:639
#, kde-format
msgid "Set"
msgstr "Velg"

#: kftabdlg.cpp:637 kftabdlg.cpp:639
#, kde-format
msgid "Do Not Set"
msgstr "Ikke velg"

#: kftabdlg.cpp:828
#, kde-format
msgctxt ""
"during the previous minute(s)/hour(s)/...; dynamic context 'type': 'i' "
"minutes, 'h' hours, 'd' days, 'm' months, 'y' years"
msgid "&during the previous"
msgid_plural "&during the previous"
msgstr[0] "i løpet av  &siste"
msgstr[1] "i løpet av  &siste"

#: kftabdlg.cpp:829
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minutt"
msgstr[1] "minutter"

#: kftabdlg.cpp:830
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "hour"
msgid_plural "hours"
msgstr[0] "time"
msgstr[1] "timer"

#: kftabdlg.cpp:831
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "day"
msgid_plural "days"
msgstr[0] "dag"
msgstr[1] "dager"

#: kftabdlg.cpp:832
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "month"
msgid_plural "months"
msgstr[0] "måned"
msgstr[1] "måneder"

#: kftabdlg.cpp:833
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "year"
msgid_plural "years"
msgstr[0] "år"
msgstr[1] "år"

#: kquery.cpp:565
#, kde-format
msgctxt "@title:window"
msgid "Error while using locate"
msgstr "Feil oppsto mens locate ble brukt"

#: main.cpp:36
#, kde-format
msgid "KFind"
msgstr "KFinn"

#: main.cpp:37
#, kde-format
msgid "KDE file find utility"
msgstr "KDE-verktøy for filfinning"

#: main.cpp:38
#, kde-format
msgid "(c) 1998-2021, The KDE Developers"
msgstr ""

#: main.cpp:40
#, kde-format
msgid "Kai Uwe Broulik"
msgstr ""

#: main.cpp:40
#, kde-format
msgid "Current Maintainer"
msgstr "Nåværende vedlikeholder"

#: main.cpp:41
#, kde-format
msgid "Eric Coquelle"
msgstr "Eric Coquelle"

#: main.cpp:41
#, kde-format
msgid "Former Maintainer"
msgstr "Tidligere vedlikeholder"

#: main.cpp:42
#, kde-format
msgid "Mark W. Webb"
msgstr "Mark W. Webb"

#: main.cpp:42
#, kde-format
msgid "Developer"
msgstr "Utvikler"

#: main.cpp:43
#, kde-format
msgid "Beppe Grimaldi"
msgstr "Beppe Grimaldi"

#: main.cpp:43
#, kde-format
msgid "UI Design & more search options"
msgstr "Grafisk design & flere søkemuligheter"

#: main.cpp:44
#, kde-format
msgid "Martin Hartig"
msgstr "Martin Hartig"

#: main.cpp:45
#, kde-format
msgid "Stephan Kulow"
msgstr "Stephan Kulow"

#: main.cpp:46
#, kde-format
msgid "Mario Weilguni"
msgstr "Mario Weilguni"

#: main.cpp:47
#, kde-format
msgid "Alex Zepeda"
msgstr "Alex Zepeda"

#: main.cpp:48
#, kde-format
msgid "Miroslav Flídr"
msgstr "Miroslav Flídr"

#: main.cpp:49
#, kde-format
msgid "Harri Porten"
msgstr "Harri Porten"

#: main.cpp:50
#, kde-format
msgid "Dima Rogozin"
msgstr "Dima Rogozin"

#: main.cpp:51
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:52
#, kde-format
msgid "Hans Petter Bieker"
msgstr "Hans Petter Bieker"

#: main.cpp:53
#, kde-format
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:53
#, kde-format
msgid "UI Design"
msgstr "UI-design"

#: main.cpp:54
#, kde-format
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:55
#, kde-format
msgid "Clarence Dang"
msgstr "Clarence Dang"

#: main.cpp:56
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Knut Yrvin"

#: main.cpp:56
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "knut.yrvin@gmail.com"

#: main.cpp:63
#, kde-format
msgid "Path(s) to search"
msgstr "Sti(er) å søke i"
